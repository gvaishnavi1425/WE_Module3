# ui.py
import pygame
import random
import strategies
import os

class GameUI:
    SCREEN_WIDTH = 1400
    SCREEN_HEIGHT = 800
    CARD_WIDTH = 100
    CARD_HEIGHT = 150
    GREEN = (0, 128, 0)

    # Define paths to card images
    CARD_IMAGE_PATHS = {
        '2♦': os.path.join('PNG-cards-1.3', '2_of_diamonds.png'),
        '3♦': os.path.join('PNG-cards-1.3', '3_of_diamonds.png'),
        '4♦': os.path.join('PNG-cards-1.3', '4_of_diamonds.png'),
        '5♦': os.path.join('PNG-cards-1.3', '5_of_diamonds.png'),
        '6♦': os.path.join('PNG-cards-1.3', '6_of_diamonds.png'),
        '7♦': os.path.join('PNG-cards-1.3', '7_of_diamonds.png'),
        '8♦': os.path.join('PNG-cards-1.3', '8_of_diamonds.png'),
        '9♦': os.path.join('PNG-cards-1.3', '9_of_diamonds.png'),
        '10♦': os.path.join('PNG-cards-1.3', '10_of_diamonds.png'),
        'J♦': os.path.join('PNG-cards-1.3', 'jack_of_diamonds.png'),
        'Q♦': os.path.join('PNG-cards-1.3', 'queen_of_diamonds.png'),
        'K♦': os.path.join('PNG-cards-1.3', 'king_of_diamonds.png'),
        'A♦': os.path.join('PNG-cards-1.3', 'ace_of_diamonds.png'),

        # Spades cards
        '2♠': os.path.join('PNG-cards-1.3', '2_of_spades.png'),
        '3♠': os.path.join('PNG-cards-1.3', '3_of_spades.png'),
        '4♠': os.path.join('PNG-cards-1.3', '4_of_spades.png'),
        '5♠': os.path.join('PNG-cards-1.3', '5_of_spades.png'),
        '6♠': os.path.join('PNG-cards-1.3', '6_of_spades.png'),
        '7♠': os.path.join('PNG-cards-1.3', '7_of_spades.png'),
        '8♠': os.path.join('PNG-cards-1.3', '8_of_spades.png'),
        '9♠': os.path.join('PNG-cards-1.3', '9_of_spades.png'),
        '10♠': os.path.join('PNG-cards-1.3', '10_of_spades.png'),
        'J♠': os.path.join('PNG-cards-1.3', 'jack_of_spades.png'),
        'Q♠': os.path.join('PNG-cards-1.3', 'queen_of_spades.png'),
        'K♠': os.path.join('PNG-cards-1.3', 'king_of_spades.png'),
        'A♠': os.path.join('PNG-cards-1.3', 'ace_of_spades.png'),

        # Clubs cards
        '2♣': os.path.join('PNG-cards-1.3', '2_of_hearts.png'),
        '3♣': os.path.join('PNG-cards-1.3', '3_of_hearts.png'),
        '4♣': os.path.join('PNG-cards-1.3', '4_of_hearts.png'),
        '5♣': os.path.join('PNG-cards-1.3', '5_of_hearts.png'),
        '6♣': os.path.join('PNG-cards-1.3', '6_of_hearts.png'),
        '7♣': os.path.join('PNG-cards-1.3', '7_of_hearts.png'),
        '8♣': os.path.join('PNG-cards-1.3', '8_of_hearts.png'),
        '9♣': os.path.join('PNG-cards-1.3', '9_of_hearts.png'),
        '10♣': os.path.join('PNG-cards-1.3', '10_of_hearts.png'),
        'J♣': os.path.join('PNG-cards-1.3', 'jack_of_hearts.png'),
        'Q♣': os.path.join('PNG-cards-1.3', 'queen_of_hearts.png'),
        'K♣': os.path.join('PNG-cards-1.3', 'king_of_hearts.png'),
        'A♣': os.path.join('PNG-cards-1.3', 'ace_of_hearts.png'),
    }

    def __init__(self, screen):
        self.screen = screen
        self.font = pygame.font.Font(None, 36)

    def welcome_message(self):
        # Display welcome message
        self.screen.fill(self.GREEN)
        self.draw_text("Welcome to Diamonds Game!", 500, 300)
        pygame.display.flip()
        pygame.time.wait(1000)

    def initial_display(self):
        # Placeholder function for initial display
        pass

    def round_setup(self, card_images):
        # Display card images
        for card_name, image_path in self.CARD_IMAGE_PATHS.items():
            card_image = pygame.image.load(image_path).convert_alpha()
            self.screen.blit(card_image, (100, 100))
            pygame.display.flip()
            pygame.time.wait(500)  # Wait for 0.5 seconds before displaying the next card

        # Placeholder function for round setup
        # Returns player bids and current card
        player1_bid = random.randint(1, 13)
        player2_bid = random.randint(1, 13)
        current_card = random.choice(list(strategies.card_values.keys()))
        return player1_bid, player2_bid, current_card

    def check_game_end(self):
        # Placeholder function to check if game has ended
        return False

    def display_winner(self, player1_score, player2_score):
        # Placeholder function to display winner
        pass

    def draw_text(self, text, x, y):
        text_surface = self.font.render(text, True, (255, 255, 255))
        text_rect = text_surface.get_rect()
        text_rect.topleft = (x, y)
        self.screen.blit(text_surface, text_rect)