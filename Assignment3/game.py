import pygame
import ui
import strategies

class Game:
    def __init__(self):
        self.player1_score = 0
        self.player2_score = 0

    def start(self):
        # Initialize Pygame
        pygame.init()

        # Set up the screen
        screen = pygame.display.set_mode((ui.GameUI.SCREEN_WIDTH, ui.GameUI.SCREEN_HEIGHT))
        pygame.display.set_caption("Diamonds Game")

        # Initialize UI
        game_ui = ui.GameUI(screen)

        # Load card images
        card_images = self.load_card_images()

        # Display welcome message
        game_ui.welcome_message()

        # Display initial setup
        game_ui.initial_display()

        # Run the game loop
        while True:
            player1_bid, player2_bid, current_card = game_ui.round_setup(card_images)

            # Update scores based on bids
            self.update_scores(player1_bid, player2_bid, current_card)

            # Check if game has ended
            if game_ui.check_game_end():
                break

        # Display winner
        game_ui.display_winner(self.player1_score, self.player2_score)

    def load_card_images(self):
        card_images = {}
        for card_name, image_path in ui.GameUI.CARD_IMAGE_PATHS.items():
            image = pygame.image.load(image_path).convert_alpha()
            card_images[card_name] = image
        return card_images


    def update_scores(self, player1_bid, player2_bid, current_card):
        if player1_bid == player2_bid:
            split_value = strategies.card_values[current_card] // 2
            self.player1_score += split_value
            self.player2_score += split_value
        elif player1_bid > player2_bid:
            self.player1_score += strategies.card_values[current_card]
        else:
            self.player2_score += strategies.card_values[current_card]

if __name__ == "__main__":
    game = Game()
    game.start()
